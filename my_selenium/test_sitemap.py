import time
import unittest
import tldextract
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select

def sitemap(driver, target):
    file = open("All_page", "w+")
    success = 0
    fail = 0
    fail_file = open("Fail_page", "w+")
    result = [] 
    result.append(target)
    success += 1
    driver.get(target)

    #st = time.time()

    domain = tldextract.extract(driver.current_url).domain

    print("[Sitemap] Sitemap on url: %s" %(target))
    urls = driver.find_elements_by_xpath("//a[@href]")
    for url in urls:
        new_url = url.get_attribute("href")
        if domain in new_url:
            new_url = new_url.rstrip('#')
            if new_url not in result:
                print(new_url)
                success += 1
                result.append(new_url)
                file.write("%s\n" % (new_url))
        else:
            fail_file.write("%s\n" % (new_url))
            fail += 1
   # et = time.time()
   # print(et-st)
    print("[Sitemap] Result on file: All_page")
    print("ok: %i, fail: %i" %(success, fail))
    file.close()
    fail_file.close()

driver = webdriver.Firefox()
sitemap(driver, "https://www.ctuet.edu.vn/")

