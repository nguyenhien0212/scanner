import time
import unittest
import tldextract
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select

driver = webdriver.Firefox()

def wait_other_url(driver, url, time=5):
    try:
        WebDriverWait(driver, time).until(lambda wd: driver.current_url != url)
        return True
    except TimeoutException:
        return False

def check_login(target):
    driver.get(target)
    if wait_other_url(driver, target):
        print("[Browser] Warning: Page is redirected to a other url.")
        print("[Browser] Please check if this is a authenticate page, please login to continue.")
        wait_other_url(driver, driver.current_url, time=10)
    cookies = driver.get_cookies()
    return driver, cookies
