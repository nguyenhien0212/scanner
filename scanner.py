#!/usr/bin/env python3

#from __future__ import print_function

from core.colors import end, red, green, white, bad, info

import pyfiglet

# Just a fancy ass banner

ascii_banner = pyfiglet.figlet_format("Scanner")

print('''%s
\n\n***********************************************************************
\n%s
\n %s This is a XSS scanning tool
\n\t Version: 1.0.0
\n https://gitlab.com/nguyenhien0212/scanner
%s\n************************************************************************\n
%s''' % (green, ascii_banner, red, green, end))

try:
    import concurrent.futures
    from urllib.parse import urlparse
    try:
        import fuzzywuzzy
    except ImportError:
        import os
        print ('%s fuzzywuzzy isn\'t installed, installing now.' % info)
        os.system('pip3 install fuzzywuzzy')
        print ('%s fuzzywuzzy has been installed, restart scanner.' % info)
        quit()
except ImportError:  # throws error in python2
    print('% Scanner isn\'t compatible with python2.\n Use python > 3.4 to run Scanner.' % bad)
    quit()

# Let's import whatever we need from standard lib
import sys
import json
import argparse

# ... and configurations core lib
import core.config
import core.log

# Processing command line arguments, where dest var names will be mapped to local vars with the same name
parser = argparse.ArgumentParser()
parser.add_argument('-u', '--url', help='url', dest='target')
parser.add_argument('-a', '--all', help='scan all page on this website', dest='allpage', action='store_true')
parser.add_argument('--data', help='post data', dest='paramData')
parser.add_argument(
    '-f', '--file', help='load payloads from a file', dest='args_file')
parser.add_argument('-e', '--encode', help='encode payloads', dest='encode')
parser.add_argument('-s', '--stored-xss', help='Scan stored xss', dest='stored_xss', action='store_true')
parser.add_argument('--timeout', help='timeout',
                    dest='timeout', type=int, default=core.config.timeout)
parser.add_argument('--json', help='treat post data as json',
                    dest='jsonData', action='store_true')
parser.add_argument('--path', help='inject payloads in the path',
                    dest='path', action='store_true')
parser.add_argument('--console-log-level', help='Console logging level',
                    dest='console_log_level', default=core.log.console_log_level,
                    choices=core.log.log_config.keys())
parser.add_argument('--file-log-level', help='File logging level', dest='file_log_level',
                    choices=core.log.log_config.keys(), default=None)
parser.add_argument('--log-file', help='Name of the file to log', dest='log_file',
                    default=core.log.log_file)
parser.add_argument('--headers', help='add headers',
        dest='add_headers', nargs='?', const=True)
parser.add_argument('-d', '--delay', help='delay between requests',
                    dest='delay', type=int, default=core.config.delay)
parser.add_argument('--skip', help='don\'t ask to continue',
                    dest='skip', action='store_true')
parser.add_argument('--proxy', help='use prox(y|ies)',
                    dest='proxy', action='store_true')
args = parser.parse_args()

# Pull all parameter values of dict from argparse namespace into local variables of name == key
# The following works, but the static checkers are too static ;-) locals().update(vars(args))
target = args.target
allpage = args.allpage
paramData = args.paramData
args_file = args.args_file
path = args.path
proxy = args.proxy
jsonData = args.jsonData
encode = args.encode
stored_xss = args.stored_xss
timeout = args.timeout
add_headers = args.add_headers
delay = args.delay
skip = args.skip
core.log.console_log_level = args.console_log_level
core.log.file_log_level = args.file_log_level
core.log.log_file = args.log_file

logger = core.log.setup_logger()
core.config.globalVariables = vars(args)

from modes.bruteforcer import bruteforcer
from modes.scan import scan
from my_selenium.check_login import check_login
from my_selenium.drawl import drawler
from my_selenium.sitemap import sitemap

from core.encoders import base64
from core.config import headers
from core.utils import extractHeaders, reader, converter
import time

print(time.time())

core.config.globalVariables['headers'] = headers
core.config.globalVariables['definitions'] = json.loads('\n'.join(reader(sys.path[0] + '/db/definitions.json')))

if args_file:
    if args_file == 'default':
        payloadList = core.config.payloads if not stored_xss else core.config.stored_payloads
    else:
        payloadList = list(filter(None, reader(args_file)))

if stored_xss:
    payloadList = core.config.stored_payloads

if path:
    paramData = converter(target, target)
elif jsonData:
    headers['Content-type'] = 'application/json'
    paramData = converter(paramData)

GET, POST = (False, True) if paramData else (True, False)

encoding = base64 if encode and encode == 'base64' else False

if not proxy:
    core.config.proxies = {}

if not target:  # if the user hasn't supplied a url
    logger.no_format('\n' + parser.format_help().lower())
    quit()

driver, cookies = check_login(target)

all_page = []
all_page.append(target)

if allpage:
    all_page = sitemap(driver, target)

for url in all_page:
    patterns = drawler(driver, url)
    if not patterns:
        continue

    for i in patterns:
        logger.info("Scan on URL: %s" % (url))
        logger.info("Scan on form: %i" % (i+1))

        pattern = patterns[i]
        logger.info("Scan on pattern: %s" % (pattern))

        GET, POST = (False, True) if (pattern["method"] == "post") else (True, False)
        paramData = pattern["params"]

        try:
             if args_file or stored_xss:
                 bruteforcer(url, paramData, payloadList, encoding, stored_xss, headers, cookies, GET, delay, timeout)
             else:
                 scan(url, paramData, encoding, headers, cookies, GET, delay, timeout, skip)
        except:
            continue
print(time.time())
driver.close()










