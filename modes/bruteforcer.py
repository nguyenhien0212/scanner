import copy
from urllib.parse import urlparse, unquote

from core.checker import checker
from core.colors import good, green, end
from core.requester import requester
from core.htmlParser import htmlParser
from core.utils import getUrl, getParams
from core.config import xsschecker, minEfficiency
from core.log import setup_logger

logger = setup_logger(__name__)


def bruteforcer(target, paramData, payloadList, encoding, stored_xss, headers, cookies, GET, delay, timeout):
    host = urlparse(target).netloc  # Extracts host out of the url
    logger.debug('Parsed host to bruteforce: {}'.format(host))
    url = getUrl(target, GET)
    logger.debug('Parsed url to bruteforce: {}'.format(url))
    params = getParams(target, paramData, GET)
    logger.debug_json('Bruteforcer params:', params)
    if not params:
        logger.error('No parameters to test.')
        quit()

    paramsCopy = copy.deepcopy(params)
    for paramName in params.keys():
        if encoding:
            paramsCopy[paramName] = encoding(xsschecker)
        else:
            paramsCopy[paramName] = xsschecker
    response = requester(url, paramsCopy, headers, cookies, GET, delay, timeout)
    occurences = htmlParser(response, encoding)
    positions = occurences.keys()
    success = 0
    for payload in payloadList:
        loggerVector = payload
        if not GET:
            payload = unquote(payload)
        efficiencies = checker(
                url, paramsCopy, headers, cookies, GET, delay, payload, positions, timeout, encoding)
        if not efficiencies:
            for i in range(len(occurences)):
                efficiencies.append(0)
        bestEfficiency = max(efficiencies)
        if bestEfficiency == 100 or (payload[0] == '\\' and bestEfficiency >= 95):
            success += 1
            logger.red_line()
            logger.good('Payload: %s' % loggerVector)
            logger.info('Efficiency: %i' % bestEfficiency)
        elif bestEfficiency > minEfficiency:
            success += 1
            logger.red_line()
            logger.good('Payload: %s' % loggerVector)
            logger.info('Efficiency: %i' % bestEfficiency)
    logger.no_format('')

    logger.info("sum: %i" % (len(payloadList)))
    logger.info("success: %i" % (success))
    logger.info("fail: %i" % (len(payloadList) - success))

    #progress = 1
    #logger.info(params)
    #paramsCopy = copy.deepcopy(params)
    #for payload in payloadList:
    #    logger.info(payload)
    #    if encoding:
    #        payload = encoding(unquote(payload))
    #    for paramName in params.keys():
    #        paramsCopy[paramName] = payload
    #    response = requester(url, paramsCopy, headers, cookies, GET, delay, timeout).text
    #    if stored_xss:
    #        payload = xsschecker
    #    if encoding:
    #        payload = encoding(payload)
    #    if payload in response:
    #        logger.info('%s %s' % (good, payload))
    #    progress += 1


    #for paramName in params.keys():
    #    progress = 1
    #    paramsCopy = copy.deepcopy(params)
    #    for payload in payloadList:
    #        logger.run('Bruteforcing %s[%s%s%s]%s: %i/%i\r' %
    #                   (green, end, paramName, green, end, progress, len(payloadList)))
    #        if encoding:
    #            payload = encoding(unquote(payload))
    #        paramsCopy[paramName] = payload
    #        response = requester(url, paramsCopy, headers,
    #                             GET, delay, timeout).text
    #        if encoding:
    #            payload = encoding(payload)
    #        if payload in response:
    #            logger.info('%s %s' % (good, payload))
    #        progress += 1
    #logger.no_format('')
